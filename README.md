> 升级背景
>
> 解决：[OpenSSH 命令注入漏洞(CVE-2020-15778)](https://blog.csdn.net/qq_41901122/article/details/114437422)

> 解决方案：升级openssl和openssh
``` shell
# 方法一、最简单的方式，直接调用下面语句，不过需要下载文件比较慢
wget https://gitee.com/believe_xin/update_ssl_ssh/raw/master/ssh_update.sh && chmod +x ssh_update.sh && ./ssh_update.sh && rm -f ./ssh_update.sh

# 方法二
git clone https://gitee.com/believe_xin/update_ssl_ssh.git
cd openssh_ssl
sh ./ssh_update_offline.sh
```

