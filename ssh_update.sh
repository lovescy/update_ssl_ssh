# 安装编译依赖包
yum install gcc gcc-c++ glibc make autoconf openssl openssl-devel pcre-devel  pam-devel pam* zlib* -y

#1、下载 openssl和ssh
#wget https://gitee.com/believe_xin/update_ssl_ssh/raw/master/openssh_ssl/openssl-1.1.1k.tar.gz
#wget https://gitee.com/believe_xin/update_ssl_ssh/raw/master/openssh_ssl/openssh-8.6p1.tar.gz
wget https://ftp.openssl.org/source/openssl-1.1.1k.tar.gz
wget https://openbsd.hk/pub/OpenBSD/OpenSSH/portable/openssh-8.6p1.tar.gz

#2、备份原文件
mv /usr/bin/openssl /usr/bin/openssl_bak
mv /usr/include/openssl /usr/include/openssl_bak

#3、安装 openssl
tar -zxvf openssl-1.1.1k.tar.gz
cd openssl-1.1.1k/
./config shared && make && make install

#4、检查编译后的文件结构

#5、软连接 openssl 目录
ln -s /usr/local/bin/openssl /usr/bin/openssl
ln -s /usr/local/include/openssl/ /usr/include/openssl

#6、加载新配置
echo "/usr/local/lib64"   >> /etc/ld.so.conf
/sbin/ldconfig

#7、查看确认版本


#8、安装openssh
cd ..
tar -zxvf openssh-8.6p1.tar.gz
cd openssh-8.6p1
chown -R root.root ./
#9、备份原 ssh 的配置文件和目录
mkdir /etc/ssh_bak/ && mv /etc/ssh/* /etc/ssh_bak/

#10、编译安装openssh
./configure --prefix=/usr/ --sysconfdir=/etc/ssh  --with-ssl-dir=/usr/local/lib64 --with-zlib   --with-md5-passwords   --with-pam --with-ssl-engine --with-selinux --with-ipaddr-display

make && make install

#11、验证查看相应的配置文件


#12、修改 sshd 配置文件,在末尾出添加，允许使用root账号登录
#vim /etc/ssh/sshd_config
#PermitRootLogin yes
echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config

#13、配置启动文件
cp -a contrib/redhat/sshd.init /etc/init.d/sshd
cp -a contrib/redhat/sshd.pam /etc/pam.d/sshd.pam
chmod +x /etc/init.d/sshd
chkconfig --add sshd
systemctl enable sshd

#14、把原先的 systemd 管理的 sshd 文件删除或者移走或者删除
mv  /usr/lib/systemd/system/sshd.service /tmp/

#15、配置开机启动
chkconfig sshd on

#16、接下来测试启停服务
/etc/init.d/sshd restart
#netstat -lntp | grep 22

#/etc/init.d/sshd stop
#/etc/init.d/sshd start
#systemctl status sshd

#17、验证版本
#ssh -V

#18、登录验证
#ssh you_username@your_server_ip

#19、如果无法登录并一直提示密码验证失败，应该是selinux导致的，解决方案是关闭selinux。
## 临时关闭，不用重启
setenforce 0
## 永久关闭
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

#20、清理安装文件
rm -f openssl-1.1.1k.tar.gz openssh-8.6p1.tar.gz
rm -rf openssl-1.1.1k openssh-8.6p1